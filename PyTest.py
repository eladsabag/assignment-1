from urllib.request import urlopen
import re
import unittest

def findString(str):
    page = urlopen("https://the-internet.herokuapp.com/context_menu").read()
    page = page.decode(encoding='utf-8', errors='strict')
    return re.findall(str,page)

class grafanaTests(unittest.TestCase):
    def test_answer1(self):
        self.assertTrue(len(findString("Right-click in the box below to see one called 'the-internet'")) > 0)

    # def test_answer2(self):
    #     self.assertTrue(len(findString("Alibaba")) > 0)

if __name__ == '__main__':
    unittest.main()


